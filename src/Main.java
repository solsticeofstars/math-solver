

import parser.EquationParser;
import solver.StackCalculator;
import testers.StackCalculatorTester;
import testers.TreeCalculatorTester;
import testers.TreeNodeTest;

public class Main {

	public static void main(String[] args) {
		testAll();
		String equation = "8+67";
		solveEquation(equation);
	}

	public static void testAll() {
		
		TreeNodeTest tnt = new TreeNodeTest();
		tnt.test();
		tnt.printTestResults();
		
		StackCalculatorTester sct = new StackCalculatorTester();
		sct.test();
		sct.printTestResults();
		
		TreeCalculatorTester tct = new TreeCalculatorTester();
		tct.test();
		tct.printTestResults();
	}
	
	public static void solveEquation(String equation) {
		EquationParser ep = new EquationParser(equation);
		ep.tokenize();
		ep.toPostfix();
		StackCalculator sc = new StackCalculator(ep.getParsedEquation());
		sc.solve();
		System.out.println(sc.getAnswer());
	}
}
