import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import structures.TriaryTree;
import utilities.StringLengthComparator;

public class TreeIndexer {

	private List<String> indexedPaths = new ArrayList<String>();
	private TriaryTree rootNode;
	
	public TreeIndexer(TriaryTree rootNode) {
		this.rootNode = rootNode;
	}
	
	public void index() {
		indexTree(rootNode,"");
		sort();
	}
	
	public void remove(String path) {
		indexedPaths.remove(path);
	}
	
	public void add(String path) {
		indexedPaths.add(path);
		sort();
	}
	
	private void sort() {
		indexedPaths.sort(new StringLengthComparator());
		Collections.reverse(indexedPaths);
	}
	private void indexTree(TriaryTree tt,String prefix) {
		if(tt.left instanceof TriaryTree) {
			String addString = prefix + "l";
			indexedPaths.add(addString);
			indexTree((TriaryTree)tt.left,addString);
		}
		if(tt.center instanceof TriaryTree) {
			String addString = prefix + "c";
			indexedPaths.add(addString);
			indexTree((TriaryTree)tt.center,addString);
		}
		if(tt.right instanceof TriaryTree) {
			String addString = prefix + "r";
			indexedPaths.add(addString);
			indexTree((TriaryTree)tt.right,addString);
		}
	}
	
	
	public void printIndex() {
		System.out.println(indexedPaths.toString());
	}
}
