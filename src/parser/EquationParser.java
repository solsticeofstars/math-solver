package parser;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EquationParser {

	private String equation;
	private List<String> parsedEquation = new ArrayList<String>();
	private Map<String,Integer> priority = new TreeMap<String,Integer>();
	private long operations;

	public EquationParser(String equation) {
		priority.put("(",5);
		priority.put("^", 4);
		priority.put("/",3);
		priority.put("*",3);
		priority.put("+", 2);
		priority.put("-", 2);
		
		
		String noSpaceEquation = equation.replaceAll("\\s","");
		this.equation = noSpaceEquation;
		//System.out.println("[EquationParser] Equation:" + this.equation);
	}

	public void tokenize() {

		String regex = "[()^*/+\\-]";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(equation);
		int previousMatch = 0;

		while(matcher.find()) {

			int startMatch = matcher.start();
			int endMatch = matcher.end();
			String match =  matcher.group();

			if(previousMatch == startMatch && match.equals("-")) {
				// its a negative number
				//System.out.println("[EquationParser] Negative number detected.");
				continue;
			} else {
				operations++;
				String matched = equation.substring(previousMatch, startMatch);
				parsedEquation.add(matched);
				parsedEquation.add(match);
			}
			previousMatch = endMatch;
		}
		parsedEquation.add(equation.substring(previousMatch));
		while(parsedEquation.contains("")) {
			parsedEquation.remove("");
		}
		//System.out.println("[EquationParser] Parsed equation: " + toString());
	}

	public void toPostfix() {
		Stack<String> opStack = new Stack<String>();
		Iterator<String> i = opStack.iterator();
		ArrayList<String> postFixEquation = new ArrayList<String>();
		
		for(String s : parsedEquation) {
			
			//System.out.println("Stack: " + opStack);
			//System.out.println("Output: " + postFixEquation.toString() + "\n");
			if(isOperator(s)){
				if(s.equals(")")) {
					while(i.hasNext() && !opStack.peek().equals("(")) {
						postFixEquation.add(opStack.pop());
					}
					opStack.pop();
				} else {
					while(i.hasNext() && priority.get(s) <= priority.get(opStack.peek()) && !opStack.peek().equals("(")) {
						postFixEquation.add(opStack.pop());
					}
					opStack.push(s);
				}
				
			} else {
				postFixEquation.add(s);
			}
		}
		
		while(i.hasNext()){
			postFixEquation.add(opStack.pop());
		}
		
		parsedEquation = postFixEquation;
	}
	
	public List<String> getParsedEquation() {
		return parsedEquation;
	}

	public String toString() {
		return parsedEquation.toString();
	}

	public long getNumberOfOperations() {
		return operations;
	}
	
	private boolean isOperator(String s) {
		return priority.containsKey(s) || s.equals("(") || s.equals(")");
	}
	
}
