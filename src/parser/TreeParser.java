package parser;

import java.util.List;
import java.util.Stack;

import utilities.Util;
import structures.TriaryTree;

public class TreeParser {

	private List<String> tokenizedEquation;
	private TriaryTree rootTree = new TriaryTree(null,null,null);

	public TreeParser(List<String> tokenizedEquation) {
		this.tokenizedEquation = tokenizedEquation;
	}

	public void parse() {
		try {
			Stack<Object> placement = new Stack<Object>();
			
			for(String s: tokenizedEquation) {
				if(Util.isNumeric(s)) {
					placement.push(s);
				} else {
					Object right = placement.pop();
					Object left = placement.pop();
					
					placement.push(new TriaryTree(left,s,right));
				}
			}
			rootTree.center = placement.pop();
		} catch (Exception e) {
			System.out.println("Not a valid equation: " + tokenizedEquation.toString());
			e.printStackTrace();
		}
	}

	public TriaryTree getEquationTree() {
		return rootTree;
	}
}
