package solver;

import java.util.List;
import java.util.Stack;
import utilities.Util;

public class StackCalculator {

	private List<String> equationList;
	private Double answer;
	
	public StackCalculator(List<String> equationList) {
		this.equationList = equationList;
	}

	public void solve() {
		Stack<String> eqStack = new Stack<String>();

		for(String s: equationList) {

			if(Util.isNumeric(s)) {
				eqStack.push(s);
			} else {
				String r = eqStack.pop();
				String l = eqStack.pop();
				eqStack.push(Util.simpleSolve(l,s,r));
			}
		}
		double ans = Double.parseDouble(eqStack.pop());
		//System.out.println("Solved: " + ans);
		answer = ans;
	}

	public String getAnswer() {
		if(answer != null) {
			return Double.toString(answer);
		} 
		return "Not solved yet.";
	}

}
