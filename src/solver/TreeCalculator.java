package solver;

import java.util.List;

import parser.TreeParser;
import structures.TriaryTree;
import utilities.Util;

public class TreeCalculator {

	private List<String> equation;
	private TriaryTree rootTree;
	private String answer;
	
	public TreeCalculator(List<String> equation) {
		this.equation = equation;
		parse();
	}
	
	private void parse() {
		TreeParser tp = new TreeParser(equation);
		tp.parse();
		rootTree = tp.getEquationTree();
	}
	
	public void solve() {
		answer = solve(rootTree);
	}
	
	public String getAnswer() {
		return answer;
	}
	
	private String solve(TriaryTree tt) {
		
		String answer = "0";
		Object[] lcrObj = new Object[]{tt.left,tt.center,tt.right};
		String[] lcr = new String[3];
		int notNull = 0;
		
		for(int i = 0; i < 3; i++) {
			if (lcrObj[i] != null) {
				if(lcrObj[i] instanceof TriaryTree) {
					lcr[i] = solve((TriaryTree)lcrObj[i]);
				} else if(lcrObj[i] instanceof String) {
					lcr[i] = (String) lcrObj[i];
				} else {
					System.out.println("Something weird happened and lcr["+i+"] is " + lcrObj[i]);
					return null;
				}
				notNull++;
			}
		}
		if(notNull == 3) {
			answer = Util.simpleSolve(lcr[0], lcr[1], lcr[2]);
		} else if (notNull == 1) {
			for(int i = 0; i < 2; i++) {
				if(lcr[i] != null) {
					answer = lcr[i];
				}
			}
		} else {
			System.out.println("Equation error:" + equation.toString() + "\nNotNull value:" + notNull + "\n");
		}
		return answer;
	}
}
