package structures;

public class TriaryTree {
	
	public Object left,center,right;
	
	public TriaryTree(Object left, Object center, Object right) {
		this.left = left;
		this.center = center;
		this.right = right;
	}

	public String print() {
		return print("");
	}
	
	private String print(String prefix) {
		
		String tree = "";
		Object[] lcr = new Object[]{left,center,right};
		
		for(int i = 0; i < lcr.length; i++) {
			if (lcr[i] != null) {
				
				boolean end = true;
				
				for(int j = i+1; j < lcr.length; j++) {
					if(lcr[j] != null) {
						end = false;
					}
				}
				
				tree += prefix + (end ? "└── ":"├── ");
				
				if(lcr[i] instanceof TriaryTree) {
					tree += "E\n";
					tree += ((TriaryTree)lcr[i]).print(prefix + (end ? "    " : "│   "));
				} else if(lcr[i] instanceof String) {
					tree += lcr[i].toString() + "\n";
				} else {
					tree += "WTF???";
				}
			}
		}

		return tree;
		
	}
	
}
