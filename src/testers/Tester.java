package testers;

import java.util.ArrayList;

public abstract class Tester {

	protected int passedTests = 0;
	protected int totalTests = 0;
	
	protected ArrayList<String> passMsg = new ArrayList<String>();
	protected ArrayList<String> failMsg = new ArrayList<String>();

	public Tester() {

	}

	public abstract void test();
	
	public abstract void testExpression(String input, String expectedResult);
	
	public int getPassedTests() {
		return passedTests;
	}
	
	public int getTotalTests() {
		return totalTests;
	}
	
	public void printTestResults() {
		System.out.format("[%s] Testing...\n\n", this.getClass().getName());
		
		System.out.format("%d passed out of %d total.\n", passedTests, totalTests);
		System.out.println("\nPassed Tests: \n");
		
		if(!passMsg.isEmpty()) {
			for(String s: passMsg) {
				System.out.println(s);
			}
		} else {
			System.out.println("None passed.");
		}
		System.out.println("\nFailed Tests: \n");
		
		if(!failMsg.isEmpty()) {
			for(String s: failMsg) {
				System.out.println(s);
			}
		} else {
			System.out.println("None failed.\n\n");
		}
		
	}
}
