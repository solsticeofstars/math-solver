package testers;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import parser.EquationParser;
import solver.StackCalculator;
import solver.TreeCalculator;

public class TreeCalculatorTester extends Tester{

	public TreeCalculatorTester() {
 
	}

	public void test() {
		testExpression("-3","-3");
		testExpression("3","3");
		testExpression("3.0","3.0");
		testExpression("-3.0","-3.0");

		testExpression("3+4","7");
		testExpression("3+4+5","12");
		testExpression("3-4","-1");
		testExpression("3-4+5","4");

		testExpression("3/4","0.75");
		testExpression("3*4*5", "60");
		testExpression("3*4/5","2.4");
		testExpression("3.0/4","0.75");

		testExpression("3^4","81");
		testExpression("1^2^3^4","1");

		testExpression("(3+4)","7");
		testExpression("(3+4)*3","21");
		testExpression("2^(3+4)","128");

	}

	public void testExpression(String start, String result) {
		totalTests++;
		try {
			EquationParser ep = new EquationParser(start);
			ep.tokenize();
			ep.toPostfix();
			
			TreeCalculator tc = new TreeCalculator(ep.getParsedEquation());
			tc.solve();

			if(Double.parseDouble(tc.getAnswer()) == Double.parseDouble(result)){
				passMsg.add("Tester passed. Expected " + result + " from equation " + start + " and got " + tc.getAnswer());
				passedTests++;
			} else {
				failMsg.add("Tester failed. Expected " + result + " from equation " + start + " and got " + tc.getAnswer());
			}
		} catch(Exception e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			failMsg.add("Tester failed. Something unexpected happened when trying: " + start + "\n" + exceptionAsString);
		}
	}
}
