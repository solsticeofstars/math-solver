package testers;
import structures.TriaryTree;

public class TreeNodeTest extends Tester {

	public TreeNodeTest() {
		
	}
	
	@Override
	public void test() {
		
		TriaryTree e = new TriaryTree(null,"e",null);
		TriaryTree root = new TriaryTree(e,null,null);
		String expectedTree = "└── E\n"
							+ "    └── e";
		testExpression(root.print(),expectedTree);
		
		TriaryTree f = new TriaryTree("f","f","f");
		root.center = f;
		
		expectedTree = "├── E\n"
					 + "│   └── e\n"
					 + "└── E\n"
					 + "    ├── f\n"
					 + "    ├── f\n"
					 + "    └── f\n";
		testExpression(root.print(),expectedTree);
		
		TriaryTree t = new TriaryTree("1","2","3");
		f.left = t;
		
		expectedTree = "├── E\n"
					 + "│   └── e\n"
					 + "└── E\n"
					 + "    ├── E\n"
					 + "    │   ├── 1\n"
					 + "    │   ├── 2\n"
					 + "    │   └── 3\n"
					 + "    ├── f\n"
					 + "    └── f\n";
		testExpression(root.print(),expectedTree);
	}

	@Override
	public void testExpression(String input, String expectedResult) {
		totalTests++;
		if(input.replaceAll("\\s+", "").equals(expectedResult.replaceAll("\\s+", ""))) {
			passedTests++;
			passMsg.add("Actual and expected same: \n\n" + expectedResult);
		} else {
			failMsg.add("Actual and expected differed: \n\n(Expected)\n" + expectedResult + "\n\nVS: \n\n(Actual)\n" + input);
		}
	}
}
