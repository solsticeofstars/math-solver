package utilities;

public class Util {

	public static boolean isNumeric(String str)
	{
		try
		{
			@SuppressWarnings("unused")
			double d = Double.parseDouble(str);
		}
		catch(NumberFormatException nfe)
		{
			return false;
		}
		return true;
	}
	
	public static String simpleSolve(String left, String op, String right) {

		double result = 0;

		try {
			double l = Double.parseDouble(left);
			double r = Double.parseDouble(right);

			if(op.equals("^")) {
				result = Math.pow(l,r);
			} else if(op.equals("*")){
				result = l * r;
			} else if(op.equals("/")){
				result = l / r;
			} else if(op.equals("+")){
				result = l + r;
			} else if(op.equals("-")){
				result = l - r;
			}
		} catch (NumberFormatException e) {
			System.out.println("Ya equation ain't valid ya idiot");
		}
		return Double.toString(result);
	}
}
